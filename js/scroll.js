const anchors = document.querySelectorAll('a[href^="#"]')

for(let anchor of anchors) {
  anchor.addEventListener("click", function(e) {
    e.preventDefault() 
    const goto = anchor.hasAttribute('href') ? anchor.getAttribute('href') : 'body'
    document.querySelector(goto).scrollIntoView({
      behavior: "smooth",
      block: "start"
    })
  })
}

$(document).mouseup(function (e) {
        var div = $(".cmn-toggle-switch__htx"); 
        if (!div.is(e.target) 
            && div.has(e.target).length === 0) { 
            div.removeClass("active")
        }
    });