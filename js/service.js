function service_item_open(x) {
    icon = x
    x = x.id 
    x = parseInt(x.match(/\d+/))

    icon = document.getElementById('service-item_icon'+x)
    mobile = document.getElementById('service-item_icon-mobile'+x)

    item = document.getElementById('service-item'+x)

    if (item.classList.contains('service-item_open')) {
        item.classList.remove('service-item_open')
        icon.classList.remove('service-item-icon_close')
        icon.classList.add('service-item-icon_open')
        mobile.classList.remove('service-item-icon_close')
        mobile.classList.add('service-item-icon_open')
    } else {
        item.classList.add('service-item_open')
        icon.classList.add('service-item-icon_close')
        icon.classList.remove('service-item-icon_open')
        mobile.classList.add('service-item-icon_close')
        mobile.classList.remove('service-item-icon_open')
    }
}