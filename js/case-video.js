const play = `<svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M33.5 20.634C34.1667 21.0189 34.1667 21.9811 33.5 22.366L17.75 31.4593C17.0833 31.8442 16.25 31.3631 16.25 30.5933L16.25 12.4067C16.25 11.6369 17.0833 11.1558 17.75 11.5407L33.5 20.634Z" fill="white"/>
</svg>
`;
const pause = `<svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect x="11" y="9.16666" width="7.33333" height="25.6667" rx="1" fill="white"/>
<rect x="25.6641" y="9.16666" width="7.33333" height="25.6667" rx="1" fill="white"/>
</svg>
`;
const sound = `<svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M7.62369 25.5395C6.31652 23.3609 6.31652 20.6391 7.62369 18.4605C8.02259 17.7957 8.68343 17.33 9.44368 17.1779L12.5476 16.5572C12.7325 16.5202 12.8992 16.421 13.0199 16.2761L19.1708 8.89498C20.3534 7.47592 20.9447 6.76638 21.4723 6.95742C22 7.14846 22 8.07207 22 9.91928L22 34.0807C22 35.9279 22 36.8515 21.4723 37.0426C20.9447 37.2336 20.3534 36.5241 19.1708 35.105L13.0199 27.7239C12.8992 27.579 12.7325 27.4798 12.5476 27.4428L9.44368 26.8221C8.68343 26.67 8.02259 26.2043 7.62369 25.5395Z" fill="white"/>
<path d="M26.6485 15.5182C28.3587 17.2284 29.3237 19.5453 29.3333 21.9639C29.3428 24.3825 28.3961 26.7069 26.6993 28.4305" stroke="white" stroke-width="2" stroke-linecap="round"/>
</svg>
`;
const mute = `<svg width="44" height="44" viewBox="0 0 46 44" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M7.62369 25.0395C6.31652 22.8609 6.31652 20.1391 7.62369 17.9605C8.02259 17.2957 8.68343 16.83 9.44368 16.6779L12.5476 16.0572C12.7325 16.0202 12.8992 15.921 13.0199 15.7761L19.1708 8.39498C20.3534 6.97592 20.9447 6.26638 21.4723 6.45742C22 6.64846 22 7.57207 22 9.41928L22 33.5807C22 35.4279 22 36.3515 21.4723 36.5426C20.9447 36.7336 20.3534 36.0241 19.1708 34.605L13.0199 27.2239C12.8992 27.079 12.7325 26.9798 12.5476 26.9428L9.44368 26.3221C8.68343 26.17 8.02259 25.7043 7.62369 25.0395Z" fill="white"/>
<rect x="31.4297" y="14.1953" width="21" height="2" rx="1" transform="rotate(45 31.4297 14.1953)" fill="white"/>
<rect x="29.8203" y="28.589" width="21" height="2" rx="1" transform="rotate(-45 29.8203 28.589)" fill="white"/>
</svg>

`;

const repeat = `<svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M18.3359 34.8333L16.9217 33.4191L15.5075 34.8333L16.9217 36.2475L18.3359 34.8333ZM24.2551 26.0858L16.9217 33.4191L19.7502 36.2475L27.0835 28.9142L24.2551 26.0858ZM16.9217 36.2475L24.2551 43.5809L27.0835 40.7525L19.7502 33.4191L16.9217 36.2475Z" fill="white"/>
<path d="M10.8886 28.4167C9.43465 25.8983 8.87907 22.9609 9.31288 20.0855C9.74669 17.2101 11.1442 14.5673 13.2764 12.59C15.4086 10.6127 18.1491 9.41822 21.049 9.20214C23.9489 8.98606 26.8362 9.76124 29.2379 11.4007C31.6396 13.0401 33.4133 15.4466 34.2685 18.226C35.1236 21.0053 35.0095 23.9927 33.9448 26.6987C32.8802 29.4047 30.928 31.6689 28.4083 33.1203C25.8885 34.5718 22.9505 35.1245 20.0755 34.6878" stroke="white" stroke-width="4" stroke-linecap="round"/>
</svg>


`;


for (i=1;i<=5;i++) {

const playButton = document.querySelector('.play-button' +i);
const playPause = document.querySelector('.play-pause' +i);
const video = document.querySelector('.video'+i);
const timeline = document.querySelector('.timeline'+i);
const soundButton = document.querySelector('.sound-button'+i);
const soundLine = document.querySelector('.sound-line'+i);
const fullscreenButton = document.querySelector('.fullscreen-button'+i);
const videoContainer = document.querySelector('.video-player'+i);
let isFullScreen = false;

playButton.addEventListener('click', function () {
  if (video.paused) {
    video.play();
    videoContainer.classList.add('playing');
    playPause.classList.add('hidden')
    playButton.innerHTML = pause;
  } else {
    video.pause();
    videoContainer.classList.remove('playing');
    playButton.innerHTML = play;
    playPause.classList.remove('hidden')
  }
})

playPause.addEventListener('click', function() {
  if (video.paused) {
    video.play();
    videoContainer.classList.add('playing');
    playPause.classList.add('hidden')
    playButton.innerHTML = pause;
  } else {
    video.pause();
    videoContainer.classList.remove('playing');
    playButton.innerHTML = play;
    playPause.classList.remove('hidden')
  }
}) 

video.addEventListener('click', function() {
  if (video.classList.contains('video_active')) {
    video.classList.remove('video_active')
    video.pause();
    playPause.classList.remove('hidden')
    playButton.innerHTML = play;
  } else {
    video.classList.add('video_active')
    video.play();
    playPause.classList.add('hidden')
    playButton.innerHTML = pause;
  }
})

video.onended = function () {
  playButton.innerHTML = repeat;
}

video.ontimeupdate = function () {
  const percentagePosition = (100*video.currentTime) / video.duration;
  timeline.style.backgroundSize = `${percentagePosition}% 100%`;
  timeline.value = percentagePosition;
  console.log(percentagePosition)
}

timeline.addEventListener('change', function () {
  const time = (timeline.value * video.duration) / 100;
  video.currentTime = time;
});

soundLine.addEventListener('change', function () {
  video.volume = soundLine.value / 100
  if (soundLine.value / 100 == 0) {
    video.muted = !video.muted;
    soundButton.innerHTML = video.muted ? mute : sound;
  } else {
    video.muted = video.unmuted;
    soundButton.innerHTML = sound;
  }
});

soundLine.addEventListener('mouseover', function() {
  soundLine.classList.add('soundline_active')
});

soundLine.addEventListener('mouseout', function() {
  soundLine.classList.remove('soundline_active')
});

soundButton.addEventListener('click', function () {
  video.muted = !video.muted;
  soundButton.innerHTML = video.muted ? mute : sound;
  if (video.muted) {
    soundLine.value = 0
  } else {
    soundLine.value = 100
  }

});

soundButton.addEventListener('mouseover', function() {
  soundLine.classList.add('soundline_active')
});

soundButton.addEventListener('mouseout', function() {
  soundLine.classList.remove('soundline_active')
});

fullscreenButton.addEventListener('click', function () {
  if (!isFullScreen) {
    if (video.requestFullscreen) {
    video.requestFullscreen();
  } else if (video.webkitRequestFullscreen) { /* Safari */
    video.webkitRequestFullscreen();
  } else if (video.msRequestFullscreen) { /* IE11 */
    video.msRequestFullscreen();
  }
  } else {
    if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.webkitExitFullscreen) { /* Safari */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) { /* IE11 */
    document.msExitFullscreen();
  }
  }
});

}

function case_close(x) {
  document.getElementById('case-video').classList.add('hidden')
}

function case_open(x) {
  x = x.id 
  x = parseInt(x.match(/\d+/))
  x = x-1
  console.log(x)

  document.getElementById('case-video').classList.remove('hidden')
  $('.case_carousel').slick('slickGoTo', x);
  $('.case_carousel').slick('refresh');
}