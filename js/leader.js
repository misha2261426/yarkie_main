function leader_item_open(x) {
    x = x.id 
    x = parseInt(x.match(/\d+/))
    icon = document.getElementById('leader-item_icon' + x)

    item = document.getElementById('leader-item'+x)

    mobile = document.getElementById('leader-item_icon-mobile'+x)

    if (item.classList.contains('leader-item_open')) {
        item.classList.remove('leader-item_open')
        mobile.classList.remove('leader-item-icon_close')
        mobile.classList.add('leader-item-icon_open')
        icon.classList.remove('leader-item-icon_close')
        icon.classList.add('leader-item-icon_open')
    } else {
        item.classList.add('leader-item_open')
        mobile.classList.add('leader-item-icon_close')
        mobile.classList.remove('leader-item-icon_open')
        icon.classList.add('leader-item-icon_close')
        icon.classList.remove('leader-item-icon_open')
    }
}

function leader_add() {
    block = document.getElementById('leader-add')
    btn = document.getElementById('leader-btn')

    block.classList.remove('leader-add_hidden')
    btn.classList.add('leader-btn_hidden')
}

// function leader_item_open_oth(x) {
//     x = x.id 
//     x = parseInt(x.match(/\d+/))
//     icon = document.getElementById('leader-item_icon' + x)

//     item = document.getElementById('leader-item'+x)

//     if (item.classList.contains('leader-item_open')) {
//         item.classList.remove('leader-item_open')
//         icon.classList.remove('leader-item-icon_close')
//         icon.classList.add('leader-item-icon_open')
//     } else {
//         item.classList.add('leader-item_open')
//         icon.classList.add('leader-item-icon_close')
//         icon.classList.remove('leader-item-icon_open')
//     }
// }