$('.case_carousel').slick({
    infinite: false,
    slidesToShow: 1,
    arrows: false,
    dots: false,
    draggable: false
});

$('.case-carousel_next').on('click', function() {
    $('.case_carousel').slick('slickNext');
});

$('.case-carousel_prev').on('click', function() {
    $('.case_carousel').slick('slickPrev');
});


// for (i=1;i<=3;i++) {
//     $('#case-play'+i).on('click', function() {
//         $('.case_carousel').slick('slickGoTo', i-1);
//     });
// }

$('#case-play1').on('click', function() {
    $('.case_carousel').slick('slickGoTo', 0);
});

$('#case-play2').on('click', function() {
    $('.case_carousel').slick('slickGoTo', 1);
});

$('#case-play3').on('click', function() {
    $('.case_carousel').slick('slickGoTo', 2);
});