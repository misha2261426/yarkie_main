function menu_btn(x) {
    menu = document.getElementById('header-menu')

    if (menu.classList.contains('header-menu_hidden')) {
        menu.classList.remove('header-menu_hidden')
        x.classList.add('header-menu-btn_close')
    } else {
        menu.classList.add('header-menu_hidden')
        x.classList.remove('header-menu-btn_close')
    }
}