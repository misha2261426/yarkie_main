function case_play_over(x) {
    x = x.id 
    x = parseInt(x.match(/\d+/))

    play = document.getElementById('case-play'+x)

    play.classList.remove('hidden')
}

function case_play_out(x) {
    x = x.id 
    x = parseInt(x.match(/\d+/))

    play = document.getElementById('case-play'+x)

    play.classList.add('hidden')
}

function case_add() {
    block = document.getElementById('case-add')
    btn = document.getElementById('case-btn')

    block.classList.remove('case-add_hidden')
    btn.classList.add('case-btn_hidden')
}